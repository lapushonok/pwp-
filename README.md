# PWP SPRING 2021
# PROJECT NAME: Meal Planner
# Group information
* Student 1. Riikka Valkama riikka.valkama97@gmail.com
* Student 2. Katri Pihlajaviita kpihlajaviita@gmail.com
* Student 3. Svetlana Pelevina pelevinasd4@gmail.com

__Remember to include all required documentation and HOWTOs, including how to create and populate the database, 
how to run and test the API, the url to the entrypoint and instructions on how to setup and run the client__

# Meal Planner

# Contents of the README

1. [Overview](README.md#overview)
2. [Database](README.md#database)
3. [API](README.md#api)
4. [Client](README.md#client)

# Overview

## Dependencies

Meal Planner has been implemented using Python 3 and various Python libraries.

Links to the sources:
- Python 3: https://www.python.org/doc/
- Flask: https://flask.palletsprojects.com/en/1.1.x/
- Flask-RESTful: https://flask-restful.readthedocs.io/en/latest/
- Flask-SQLAlchemy: https://flask-sqlalchemy.palletsprojects.com/en/2.x/
- SQLAlchemy: https://www.sqlalchemy.org/
- Pytest: https://docs.pytest.org/en/stable/
- JSONSchema: https://python-jsonschema.readthedocs.io/en/stable/
- Pytest-cov: https://pytest-cov.readthedocs.io/en/latest/


The dependencies outside Python Standard Library can be found in the project ``requirements.txt`` file.

## Setup

### Python

Python 3 is required for usage of Meal Planner.

Python 3 can be installed from: https://www.python.org/downloads/

### Install dependencies

All given dependencies must be installed before deploying and utilizing the project.
The needed dependencies are declared in ``requirements.txt``.

To install the dependencies, locate the ``requirements.txt`` file, and run the following command:
```
pip install -r /path/to/requirements.txt
```

where ``/path/to/requirements.txt`` should be replaced with the location of the ``requirements.txt``-file.

# Database

For the Meal Planner database implementation, Python libraries Flask-SQLAlchemy and Flask have been utilized.

## How to use

The contents of the database can be accessed in a Python environment. A Python console, e.g., IPython, can be used
for this purpose as shown below.

1. Install IPython with pip:
```
pip install IPython
```

2. Locate the project files of Meal Planner. Preferably, open a terminal from the location of the file ``meal_planner/api.py``.
The database file ```meal_planner.db``` should be available in the same directory in the folder ``db/``.
   
3. Open a terminal from the location of ``api.py``. Launch IPython by running the following command:
```
IPython
```
or
```
python -m IPython
```

4. Import the database and the models in the environment by running:
```
from meal_planner.api import db, app 
from meal_planner.db.models import IngredientTypeEnum, Ingredient, Recipe, RecipeItem, User
```

5. Create the database by running:
```
db.create_all()
```

It should now be possible the query and access the contents of the database.

## Database queries

More information on the queries can be found at: https://docs.sqlalchemy.org/en/13/orm/tutorial.html#querying

## Modifying the database

When creating new entities, they should be added to the session with the command:
```
db.session.add(<Entity>)
```
To make the changes effective in the database, the following command should be run:
```
db.session.commit()
```

## Database tests

To test the database works properly, open terminal from the project directory and run the specific file using pytest

```
pytest meal_planner/tests/db_test.py
```

Alternative way to run all tests is to execute command
```
pytest 
```

Currently, the database contains 8 unit tests in db_test.py.

# API

Meal Planner REST API has been implemented using Python libraries Flask and Flask-RESTful.

## How to use

To deploy the Flask server of Meal Planner, run the file ``meal_planner/api.py``, e.g., from project root folder:
```
python -m meal_planner.api
```

If successful, the API can be accessed from the assigned address. By default, the API entry point can be accessed at 
``http://127.0.0.1:5000/api/``.

## API documentation

The comprehensive documentation for Meal Planner API is available at: [https://mealplanner1.docs.apiary.io/#](https://mealplanner1.docs.apiary.io/#)

## API tests

For testing the API implementation, the following test files are available:
- test_api_entry.py
- test_api_user.py
- test_api_favourite_recipe.py
- test_api_recipe.py
- test_api_recipe_item.py
- test_api_ingredient.py

To run a specific test file, open terminal from the project root directory and run:
```
pytest meal_planner/tests/[filename]
```

where ``[filename]`` should be replaced with the desired filename.

Alternative way to run all tests is to execute the command:
```
pytest 
```
or
```
python -m pytest
```
To view the coverage of the tests, run the following command:
```
pytest --cov-report term-missing --cov=meal_planner meal_planner/tests/
```
The current coverage of the created tests is 99%.  For reference, the results of the tests can be seen in the figure below:
![](images/pytest_results_220421.PNG)

# Client

The client of the project demonstrates the content of the API, where the client utilises three resources from the API: Recipe Collection, Ingredient Collection and Recipe resources. For the use, the client demands that the API is running locally on PC. Check from the section, [API](README.md#api), how to start the API. 

After the API is running on PC, the client is available at ``http://127.0.0.1:5000/client/``. The client enables to add new recipes and ingredients into API and it shows all existing instances behind the links. In addition, the client provides an opportunity to modify a specific recipe instance or delete it from the API.
