# Meetings notes

## Meeting 1.
* **DATE:** 11.02.2021
* **ASSISTANTS:** Mika Oja

### Minutes

Contents of the meeting:
1. Share the motivation behind the topic.
2. Comments on specific sections of DL1:
    - Overview
        - Content ok, no need for changes
    - Main concepts and relations
        - Concepts and relations ok (as textually described), no need for changes
        - Relations not clearly depicted in the diagram (see Action points)
    - API uses
        - Content ok
        - Two more practical examples need to be given
    - Related work
        - Content ok
        - Minor structural imporvements needed (see Action points)

### Action points

Action points for the following sections of DL1:
- Main concpets and relations
    - Modify the diagram to show the relations
    - Add "day" as a concept
- API uses
    - Add two more concrete examples
        - short descriptions for both
- Related work
    - Structural change to the text:
        - Move API classification pharagraph before the description of the API features / after the first pharagraph


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 2.
* **DATE:** 25.02.2021
* **ASSISTANTS:** Mika Oja

### Minutes

Contents of the meeting:

1. Comments on specific sections of DL2:
    - Database design
        - Ok overall
        - No foreign key for many to many-relations (see Action points)
    - Database implementation
        - Ok overall
        - Behaviour at deletion should be implemented (see Action points)
    - Database testing
        - Ok overall
        - Behaviour at deletion should be tested (see Action points)
        
2. Current work is quite extensive, and too much work for the rest of course as is (see Action points).

### Action points
1. Action points for the following sections of DL2:
    - Database design
        - Remove many to many -relations from the tables
    - Database implementation
        - Implement behaviour at delete for relations and foreign keys
    - Database testing
        - Test behaviour at delete for relations and foreign keys
2. Other:
    - Scale down the project size
        - i.e. the number of the concepts/database tables/API resources should be decreased


### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 3.
* **DATE:** 18.03.2021
* **ASSISTANTS:** Mika Oja

### Minutes

Contents of the meeting:

1. Comments on specific sections of DL3:
    - Resources and relations
        - Resource table: ok overall
        - see Action points
    - Uniform interface
        - Ok overall
    - Api design
        - See Action points
            - Single resource -> no items, only the user itself at the root level
            - No "instruction" in "items" (collection GET)
            - FavouriteRecipe can be link resource
                - GET not necessary for favourite recipe
                - FavouriteRecipe list should have a delete link   
            - Responses should have response headers (location) for newly created items
    - REST conformance
        - Ok overall
        - see Action points

### Action points

1. Action points for the following sections of DL3:
    - Resources and relations
        - Add a link from Favouriterecipecollection back to User
        - Re-evaluate: top level links to needed resources?
    - Api design
        - Implement updates from action points for Resources and relations 
        - Remove "items" from single resources
        - Remove instruction from RecipeCollection GET
        - Add location headers to responses for newly created items
        - Re-evaluate: no separate GET for FavouriterecipeCollection?
    - REST conformance
        - Specify how GET, POST, PUT, DELETE used in the API
2. Other
    - No need to rework previous deadlines

### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Meeting 4.
* **DATE:**  14.04.2021
* **ASSISTANTS:** Mika Oja

### Minutes

Contents of the meeting:

1. Comments on specific sections of DL4:
    - List of implemented resources
        - Ok
    - Resources implementation
        - Overall, code is clean, the implementation looks good
        - See action points
    - RESTful API testing
        - No need for modifications as the test coverage is 100% for the relevant code

### Action points

Action points for the following sections of DL4:

- Resources implementation
    - Add more details to the function docstrings
    - Add missign links to source materials to the relevant code files

### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Midterm meeting
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*

### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

## Final meeting
* **DATE:**
* **ASSISTANTS:**

### Minutes
*Summary of what was discussed during the meeting*

### Action points
*List here the actions points discussed with assistants*

### Comments from staff
*ONLY USED BY COURSE STAFF: Additional comments from the course staff*

