"""Module for Meal Planner Builder.

The following code is based on:
- Programmable Web Project -course Exercise 3 available at:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""
from flask import url_for

from meal_planner.utils.masonbuilder import MasonBuilder
from meal_planner.db.models import IngredientTypeEnum

ingredient_type = IngredientTypeEnum


class MealPlannerBuilder(MasonBuilder):
    """Class for Meal Planner Builder controls. Extends MasonBuilder."""

    @staticmethod
    def recipe_schema():
        """Schema for RecipeResourceItem."""
        schema = {
            "type": "object",
            "required": ["name", "instruction", "main_ingredient_type"]
        }
        props = schema["properties"] = {}
        props["name"] = {
            "description": "Recipe name.",
            "type": "string",
            "minLength": 1
        }
        props["instruction"] = {
            "description": "Recipe instruction.",
            "type": "string",
            "minLength": 1
        }
        props["main_ingredient_type"] = {
            "description": "Recipe main ingredient type.",
            "enum": ['berry', 'dairy', 'egg', 'fish', 'fruit', 'grain', 'poultry', 'red_meat',
                     'rice', 'sausage', 'seafood', 'vegetable', 'miscellaneous']
        }
        return schema

    @staticmethod
    def favourite_recipe_schema():
        """Schema for FavouriteRecipe."""
        schema = {
            "type": "object",
            "required": ["recipe_name"]
        }
        props = schema["properties"] = {}
        props["recipe_name"] = {
            "description": "The name of the recipe",
            "type": "string",
            "minLength": 1
        }
        return schema

    @staticmethod
    def user_schema():
        """Schema for UserResourceItem."""
        schema = {
            "type": "object",
            "required": ["name"]
        }
        props = schema["properties"] = {}
        props["name"] = {
            "description": "The name of the user",
            "type": "string",
            "minLength": 1
        }
        return schema

    @staticmethod
    def recipe_item_schema():
        """Schema for RecipeItemResourceItem."""
        schema = {
            "type": "object",
            "required": ["ingredient", "amount", "measure"]
        }
        props = schema["properties"] = {}
        props["name"] = {
            "description": "Name of the recipe item ingredient",
            "type": "string",
            "minLength": 1
        }
        props["ingredient"] = {
            "description": "Quantity for the ingredient",
            "type": "string",
            "minLength": 1
        }
        props["amount"] = {
            "description": "Amount of the ingredient in the recipe item",
            "type": "number"
        }
        props["measure"] = {
            "description": "Unit of measurement",
            "type": "string",
            "minLength": 1
        }
        return schema

    @staticmethod
    def ingredient_schema():
        """Schema for IngredientResourceItem."""
        schema = {
            "type": "object",
            "required": ["name", "type"]
        }
        props = schema["properties"] = {}
        props["name"] = {
            "description": "Name of the ingredient",
            "type": "string",
            "minLength": 1
        }
        props["calories"] = {
            "description": "Calorie count of the ingredient",
            "type": "number"
        }
        props["type"] = {
            "description": "Type of the ingredient. One value from a set of predefined types",
            "enum": ['berry', 'dairy', 'egg', 'fish', 'fruit', 'grain', 'poultry', 'red_meat',
                     'rice', 'sausage', 'seafood', 'vegetable', 'miscellaneous']
        }
        return schema

    def add_control_add_recipe(self):
        """Method for adding "mealplanner:add-recipe"-control to a MASON JSON body."""
        self.add_control("mealplanner:add-recipe",
                         url_for("api.recipecollection"),
                         method="POST",
                         title="Add a recipe to the recipe collection.",
                         encoding="json",
                         schema=self.recipe_schema()
                         )

    def add_control_recipes(self):
        """Method for adding "mealplanner:recipes-all"-control to a MASON JSON body."""
        self.add_control("mealplanner:recipes-all",
                         url_for("api.recipecollection"),
                         title="All recipes in API"
                         )

    def add_control_edit_recipe(self, recipe):
        """Method for adding edit-control to a MASON JSON body for Recipe-resource."""
        self.add_control("edit",
                         url_for("api.reciperesourceitem", recipe=recipe),
                         method="PUT",
                         encoding="json",
                         schema=self.recipe_schema()
                         )

    def add_control_delete_recipe(self, recipe):
        """Method for adding "mealplanner:delete"-control to a MASON JSON body for Recipe-resource."""
        self.add_control("mealplanner:delete",
                         url_for("api.reciperesourceitem", recipe=recipe),
                         method="DELETE"
                         )

    def add_control_add_user(self):
        """Method for adding "mealplanner:add-user"-control to a MASON JSON body for UserCollection-resource."""
        self.add_control("mealplanner:add-user",
                         url_for("api.usercollection"),
                         method="POST",
                         encoding="json",
                         title="Add a user to a user collection",
                         schema=self.user_schema()
                         )

    def add_control_users(self):
        """Method for adding "mealplanner:users-all"-control to a MASON JSON body."""
        self.add_control("mealplanner:users-all",
                         url_for("api.usercollection"),
                         title="All users in API"
                         )

    def add_control_edit_user(self, user):
        """Method for adding edit-control to a MASON JSON body for User-resource."""
        self.add_control("edit",
                         url_for("api.userresourceitem", user=user),
                         method="PUT",
                         encoding="json",
                         title="Edit this user",
                         schema=self.user_schema()
                         )

    def add_control_delete_user(self, user):
        """Method for adding "mealplanner:delete"-control to a MASON JSON body for User-resource."""
        self.add_control("mealplanner:delete",
                         url_for("api.userresourceitem", user=user),
                         title="Delete this user",
                         method="DELETE"
                         )

    def add_control_favourite_recipes(self, user):
        """Method for adding "mealplanner:favourite_recipes-all"-control to a MASON JSON body."""
        self.add_control("mealplanner:favourite_recipes-all",
                         url_for("api.favouriterecipecollection", user=user),
                         title="All user's favourite recipes"
                         )

    def add_control_add_favourite_recipe(self, user):
        """Method for adding "mealplanner:add-favourite_recipe"-control to a MASON JSON body for
        FavouriteRecipeCollection-resource.
        """
        self.add_control("mealplanner:add-favourite_recipe",
                         url_for("api.favouriterecipecollection", user=user),
                         method="POST",
                         encoding="json",
                         title="Add a recipe to a user's favourite recipes collection.",
                         schema=self.favourite_recipe_schema()
                         )

    def add_control_delete_favourite_recipe(self, user, recipe):
        """Method for adding "mealplanner:delete"-control to a MASON JSON body for FavouriteRecipe-resource."""
        self.add_control("mealplanner:delete",
                         url_for("api.favouritereciperesourceitem", user=user, recipe=recipe),
                         title="Delete this favourite recipe from user's favourite recipes.",
                         method="DELETE"
                         )

    def add_control_recipe_items(self, recipe_name):
        """Method for adding "mealplanner:recipe_items-all"-control to a MASON JSON body."""
        self.add_control("mealplanner:recipe_items-all",
                         url_for("api.recipeitemscollection", recipe=recipe_name),
                         title="All recipe items for the recipe"
                         )

    def add_control_add_recipe_item(self, recipe_name):
        """Method for adding "mealplanner:add-recipe-item"-control to a MASON JSON body for
        RecipeItemCollection-resource.
        """
        self.add_control("mealplanner:add-recipe-item",
                         url_for("api.recipeitemscollection", recipe=recipe_name),
                         method="POST",
                         encoding="json",
                         title="Add a recipe item to the recipe.",
                         schema=self.recipe_item_schema()
                         )

    def add_control_edit_recipe_item(self, recipe_name, ingredient_name):
        """Method for adding "edit"-control to a MASON JSON body for RecipeItem-resource."""
        self.add_control("edit",
                         url_for("api.recipeitemresourceitem", recipe=recipe_name, recipeitem=ingredient_name),
                         method="PUT",
                         encoding="json",
                         schema=self.recipe_item_schema()
                         )

    def add_control_delete_recipe_item(self, recipe_name, ingredient_name):
        """Method for adding "mealplanner:delete"-control to a MASON JSON body for RecipeItem-resource."""
        self.add_control("mealplanner:delete",
                         url_for("api.recipeitemresourceitem", recipe=recipe_name, recipeitem=ingredient_name),
                         method="DELETE"
                         )

    def add_control_ingredients(self):
        """Method for adding "mealplanner:ingredients-all"-control to a MASON JSON body."""
        self.add_control("mealplanner:ingredients-all",
                         url_for("api.ingredientcollection"),
                         title="All ingredients in API"
                         )

    def add_control_add_ingredient(self,):
        """Method for adding "mealplanner:add-ingredient"-control to a MASON JSON body for
        IngredientCollection-resource.
        """
        self.add_control("mealplanner:add-ingredient",
                         url_for("api.ingredientcollection"),
                         method="POST",
                         encoding="json",
                         title="Add a ingredient to the ingredient collection.",
                         schema=self.ingredient_schema()
                         )

    def add_control_edit_ingredient(self, ingredient_name):
        """Method for adding "edit"-control to a MASON JSON body for Ingredient-resource."""
        self.add_control("edit",
                         url_for("api.ingredientresourceitem", ingredient=ingredient_name),
                         method="PUT",
                         encoding="json",
                         schema=self.ingredient_schema()
                         )

    def add_control_delete_ingredient(self, ingredient_name):
        """Method for adding "mealplanner:delete"-control to a MASON JSON body for Ingredient-resource."""
        self.add_control("mealplanner:delete",
                         url_for("api.ingredientresourceitem", ingredient=ingredient_name),
                         method="DELETE"
                         )

    def add_control_ingredient_info(self, ingredient_name):
        """Method for adding "mealplanner:ingredient_info"-control to a MASON JSON body for RecipeItem-resource."""
        self.add_control("mealplanner:ingredient_info",
                         url_for("api.ingredientresourceitem", ingredient=ingredient_name),
                         encoding="json",
                         title="Get an ingredient for current recipe item."
                         )
