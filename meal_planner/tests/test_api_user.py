"""Module for User and UserCollection resource tests.

The following code is based on:
- Programmable Web project course Exercise 3 Testing Flask Applications::
  https://lovelace.oulu.fi/ohjelmoitava-web/programmable-web-project-spring-2019/testing-flask-applications-part-2/
"""

import json
import os
import tempfile

import pytest
from jsonschema import validate
from sqlalchemy.engine import Engine
from sqlalchemy import event

from meal_planner.api import app, db
from meal_planner.db.models import User


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


@pytest.fixture
def client():
    db_fd, db_fname = tempfile.mkstemp()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + db_fname
    app.config["TESTING"] = True

    with app.app_context():
        db.create_all()
    _populate_db()

    yield app.test_client()

    db.session.remove()
    os.close(db_fd)
    os.unlink(db_fname)


def _populate_db():
    """Populates the database with valid entities for testing."""
    for i in range(1, 4):
        user = User(
            name="test-user-{}".format(i),
        )
        db.session.add(user)
    db.session.commit()


def _get_user_json(number):
    """Creates a valid user JSON for PUT and POST tests."""
    return {"name": "extra-user-{}".format(number)}


def _get_invalid_user_json(number):
    """Creates an invalid user JSON for PUT and POST tests."""
    return {"invalid_name": "extra-user-{}".format(number)}


def _check_namespace(client, response):
    """Checks that the "mealplanner" namespace is found from the response body.
    Checks that its "name" attribute is a URL that can be accessed.
    """
    ns_href = response["@namespaces"]["mealplanner"]["name"]
    resp = client.get(ns_href)
    assert resp.status_code == 302


def _check_profile_control(ctrl, client, obj):
    """Checks a GET type control from a JSON object for a profile control.
    Checks that the URL of the control can be accessed.
    Checks that the correct status code is returned (302).
    """
    href = obj["@controls"][ctrl]["href"]
    resp = client.get(href)
    assert resp.status_code == 302


def _check_control_get_method(ctrl, client, obj):
    """Checks a GET type control from a JSON object.
    Checks that the URL of the control can be accessed.
    Checks that the correct status code is returned (200).
    """
    href = obj["@controls"][ctrl]["href"]
    resp = client.get(href)
    assert resp.status_code == 200


def _check_control_post_method(ctrl, client, obj):
    """Checks a POST type control from a JSON object.
    Checks that fields "href", "method", "encoding", and "schema" are found from the control object.
    Validates the schema from the control object against the valid schema.
    Checks that using the control results in the correct status code of 201.
    """
    ctrl_obj = obj["@controls"][ctrl]
    href = ctrl_obj["href"]
    method = ctrl_obj["method"].lower()
    encoding = ctrl_obj["encoding"].lower()
    schema = ctrl_obj["schema"]
    assert method == "post"
    assert encoding == "json"
    body = _get_user_json(number=1)
    validate(body, schema)
    resp = client.post(href, json=body)
    assert resp.status_code == 201


def _check_control_put_method(ctrl, client, obj):
    """Checks a PUT type control from a JSON object.
    Checks that fields "href", "method", "encoding", and "schema" are found from the control object.
    Validates the schema from the control object against the valid schema.
    Checks that using the control results in the correct status code of 204.
    """
    ctrl_obj = obj["@controls"][ctrl]
    href = ctrl_obj["href"]
    method = ctrl_obj["method"].lower()
    encoding = ctrl_obj["encoding"].lower()
    schema = ctrl_obj["schema"]
    assert method == "put"
    assert encoding == "json"
    body = _get_user_json(number=1)
    body["name"] = obj["name"]
    validate(body, schema)
    resp = client.put(href, json=body)
    assert resp.status_code == 204


def _check_control_delete_method(ctrl, client, obj):
    """Checks a DELETE type control from a JSON object.
    Checks the control's method in addition to its "href".
    Checks that using the control results in the correct status code of 204.
    """
    href = obj["@controls"][ctrl]["href"]
    method = obj["@controls"][ctrl]["method"].lower()
    assert method == "delete"
    resp = client.delete(href)
    assert resp.status_code == 204


class TestUserCollection(object):
    """Class for implementations of tests for each HTTP method in user collection."""

    RESOURCE_URL = "/api/users/"

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)
        _check_namespace(client, body)
        _check_control_get_method("mealplanner:ingredients-all", client, body)
        _check_control_get_method("mealplanner:recipes-all", client, body)
        _check_control_post_method("mealplanner:add-user", client, body)
        assert len(body["items"]) == 3
        for item in body["items"]:
            _check_control_get_method("self", client, item)
            _check_profile_control("profile", client, item)

    def test_post(self, client):
        valid = _get_user_json(1)
        invalid = _get_invalid_user_json(number=1)

        # test with wrong content type
        resp = client.post(self.RESOURCE_URL, data=json.dumps(valid))
        assert resp.status_code == 415

        # send invalid body for 400
        resp = client.post(self.RESOURCE_URL, json=invalid)
        assert resp.status_code == 400

        # test with valid and see that it exists afterward
        resp = client.post(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 201
        assert resp.headers["Location"].endswith(self.RESOURCE_URL + valid["name"] + "/")
        resp = client.get(resp.headers["Location"])
        assert resp.status_code == 200

        # send same data again for 409
        resp = client.post(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 409


class TestUserResourceItem:
    """Class for implementations of tests for each HTTP method in user resource item."""

    RESOURCE_URL = "/api/users/test-user-1/"
    INVALID_URL = "/api/users/non-existent-user/"
    MODIFIED_URL = "/api/users/modified-user-1/"

    def test_get(self, client):
        """Tests the GET method for User resource item.
        Checks that the request returns a correct status code (200).
        Checks that all of the expected attributes and controls are present, and that the controls work.
        Checks that all of the items from the DB population are present, and their controls.
        """
        # test to get user does exist
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)
        assert body["name"] == "test-user-1"
        _check_namespace(client, body)
        _check_profile_control("profile", client, body)
        _check_control_get_method("mealplanner:favourite_recipes-all", client, body)
        _check_control_get_method("collection", client, body)
        _check_control_put_method("edit", client, body)
        _check_control_delete_method("mealplanner:delete", client, body)

        # test to get user does not exist
        resp = client.get(self.INVALID_URL)
        assert resp.status_code == 404

    def test_put(self, client):
        """Tests the PUT method for User resource item.
        Checks that a valid request receives a 204 response.
        Checks that when name is changed, the user can be found from a its new URI.
        Checks all of the possible error codes.
        """

        valid = _get_user_json(number=1)
        invalid = _get_invalid_user_json(number=1)

        # test with wrong content type
        resp = client.put(self.RESOURCE_URL, data=json.dumps(valid))
        assert resp.status_code == 415

        # send invalid body for 400
        resp = client.put(self.RESOURCE_URL, json=invalid)
        assert resp.status_code == 400

        # test with user does not exist
        resp = client.put(self.INVALID_URL, json=valid)
        assert resp.status_code == 404

        # test with another user's name
        valid["name"] = "test-user-2"
        resp = client.put(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 409

        # test with valid
        valid["name"] = "modified-user-1"
        resp = client.put(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 204

    def test_delete(self, client):
        """Tests the DELETE method for User resource item.
        Checks that a valid request receives the correct status code (204).
        Checks all of the possible error codes.
        """
        resp = client.delete(self.RESOURCE_URL)
        assert resp.status_code == 204
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 404
        resp = client.delete(self.INVALID_URL)
        assert resp.status_code == 404
