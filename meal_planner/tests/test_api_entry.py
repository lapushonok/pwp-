"""Module for User and UserCollection resource tests.

The following code is based on:
- Programmable Web project course Exercise 3 Testing Flask Applications::
  https://lovelace.oulu.fi/ohjelmoitava-web/programmable-web-project-spring-2019/testing-flask-applications-part-2/
"""
import json
import os
import tempfile

import pytest
from sqlalchemy.engine import Engine
from sqlalchemy import event

from meal_planner.api import app, db


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


@pytest.fixture
def client():
    db_fd, db_fname = tempfile.mkstemp()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + db_fname
    app.config["TESTING"] = True

    with app.app_context():
        db.create_all()

    yield app.test_client()

    db.session.remove()
    os.close(db_fd)
    os.unlink(db_fname)


def _check_namespace(client, response):
    """Checks that the "mealplanner" namespace is found from the response body.
    Checks that its "name" attribute is a URL that can be accessed.
    """
    ns_href = response["@namespaces"]["mealplanner"]["name"]
    resp = client.get(ns_href)
    assert resp.status_code == 302


def _check_control_get_method(ctrl, client, obj):
    """Checks a GET type control from a JSON object.
    Checks that the URL of the control can be accessed.
    Checks that the correct status code is returned (200).
    """
    href = obj["@controls"][ctrl]["href"]
    resp = client.get(href)
    assert resp.status_code == 200


class TestEntry(object):
    """Class for implementations of Entry tests"""
    RESOURCE_URL = "/api/"

    def test_get(self, client):
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)
        _check_namespace(client, body)
        _check_control_get_method("mealplanner:users-all", client, body)
        _check_control_get_method("mealplanner:recipes-all", client, body)
        _check_control_get_method("mealplanner:ingredients-all", client, body)
