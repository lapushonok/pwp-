"""Module for Recipe and RecipeCollection resource tests.

The following code is based on:
- Programmable Web project course Exercise 3 Testing Flask Applications::
  https://lovelace.oulu.fi/ohjelmoitava-web/programmable-web-project-spring-2019/testing-flask-applications-part-2/
"""
import json
import os
import tempfile

import pytest
from jsonschema import validate
from sqlalchemy.engine import Engine
from sqlalchemy import event

from meal_planner.api import app, db
from meal_planner.db.models import Recipe


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


@pytest.fixture
def client():
    db_fd, db_fname = tempfile.mkstemp()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + db_fname
    app.config["TESTING"] = True

    with app.app_context():
        db.create_all()
    _populate_db()

    yield app.test_client()

    db.session.remove()
    os.close(db_fd)
    os.unlink(db_fname)


def _populate_db():
    """Populates the database with valid entities for testing."""
    for i in range(1, 4):
        recipe = Recipe(
            name="test-recipe-{}".format(i),
            instruction="testinstructions",
            main_ingredient_type="dairy"
        )
        db.session.add(recipe)
    db.session.commit()


def _get_recipe_json(number, main_ingredient_type):
    """Creates a valid recipe JSON for PUT and POST tests."""
    return {"name": "extra-recipe-{}".format(number),
            "instruction": "testinstructions",
            "main_ingredient_type": main_ingredient_type}


def _check_namespace(client, response):
    """Checks that the "mealplanner" namespace is found from the response body.
    Checks that its "name" attribute is a URL that can be accessed.
    """
    ns_href = response["@namespaces"]["mealplanner"]["name"]
    resp = client.get(ns_href)
    assert resp.status_code == 302


def _check_control_get_method(ctrl, client, obj):
    """Checks a GET type control from a JSON object.
    Checks that the URL of the control can be accessed.
    Checks that the correct status code is returned (200).
    """
    href = obj["@controls"][ctrl]["href"]
    resp = client.get(href)
    assert resp.status_code == 200


def _check_profile_control(ctrl, client, obj):
    """Checks a GET type control from a JSON object for a profile control.
    Checks that the URL of the control can be accessed.
    Checks that the correct status code is returned (302).
    """
    href = obj["@controls"][ctrl]["href"]
    resp = client.get(href)
    assert resp.status_code == 302


def _check_control_post_method(ctrl, client, obj):
    """Checks a POST type control from a JSON object.
    Checks that fields "href", "method", "encoding", and "schema" are found from the control object.
    Validates the schema from the control object against the valid schema.
    Checks that using the control results in the correct status code of 201.
    """
    ctrl_obj = obj["@controls"][ctrl]
    href = ctrl_obj["href"]
    method = ctrl_obj["method"].lower()
    encoding = ctrl_obj["encoding"].lower()
    schema = ctrl_obj["schema"]
    assert method == "post"
    assert encoding == "json"
    body = _get_recipe_json(number=1, main_ingredient_type="dairy")
    validate(body, schema)
    resp = client.post(href, json=body)
    assert resp.status_code == 201


def _check_control_put_method(ctrl, client, obj):
    """Checks a PUT type control from a JSON object.
    Checks that fields "href", "method", "encoding", and "schema" are found from the control object.
    Validates the schema from the control object against the valid schema.
    Checks that using the control results in the correct status code of 204.
    """
    ctrl_obj = obj["@controls"][ctrl]
    href = ctrl_obj["href"]
    method = ctrl_obj["method"].lower()
    encoding = ctrl_obj["encoding"].lower()
    schema = ctrl_obj["schema"]
    assert method == "put"
    assert encoding == "json"
    body = _get_recipe_json(number=1, main_ingredient_type="dairy")
    body["name"] = obj["name"]
    validate(body, schema)
    resp = client.put(href, json=body)
    assert resp.status_code == 204


def _check_control_delete_method(ctrl, client, obj):
    """Checks a DELETE type control from a JSON object.
    Checks the control's method in addition to its "href".
    Checks that using the control results in the correct status code of 204.
    """
    href = obj["@controls"][ctrl]["href"]
    method = obj["@controls"][ctrl]["method"].lower()
    assert method == "delete"
    resp = client.delete(href)
    assert resp.status_code == 204


class TestRecipeCollection:
    """Class for implementations of tests for each HTTP method in recipe collection resource."""

    RESOURCE_URL = "/api/recipes/"

    def test_get(self, client):
        """Tests the GET method for Recipe collection.
        Checks that the request returns a correct status code (200).
        Checks that all of the expected attributes and controls are present, and that the controls work.
        Checks that all of the items from the DB population are present, and their controls.
        """
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)
        _check_namespace(client, body)
        _check_control_get_method("mealplanner:users-all", client, body)
        _check_control_get_method("mealplanner:ingredients-all", client, body)
        _check_control_post_method("mealplanner:add-recipe", client, body)
        assert len(body["items"]) == 3
        for item in body["items"]:
            _check_control_get_method("self", client, item)
            _check_profile_control("profile", client, item)
            assert "name" in item
            assert "main_ingredient_type" in item

    def test_post(self, client):
        """Tests the POST method for Recipe collection.
        Checks that a valid request receives a 201 response with a
        location header that leads into the newly created resource.
        Checks all of the possible error codes.
        """
        valid = _get_recipe_json(number=1, main_ingredient_type="dairy")

        # test with wrong content type
        resp = client.post(self.RESOURCE_URL, data=json.dumps(valid))
        assert resp.status_code == 415

        # test with valid and see that it exists afterward
        resp = client.post(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 201
        assert resp.headers["Location"].endswith(self.RESOURCE_URL + valid["name"] + "/")
        resp = client.get(resp.headers["Location"])
        assert resp.status_code == 200
        body = json.loads(resp.data)
        assert body["name"] == "extra-recipe-1"
        assert body["instruction"] == "testinstructions"

        # send same data again for 409
        resp = client.post(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 409

        # remove instruction field for 400
        valid.pop("instruction")
        resp = client.post(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 400


class TestRecipeResourceItem:
    """Class for implementations of tests for each HTTP method in recipe resource item."""

    RESOURCE_URL = "/api/recipes/test-recipe-1/"
    INVALID_URL = "/api/recipes/non-existent-recipe/"
    MODIFIED_URL = "/api/recipes/modified-recipe-1/"

    def test_get(self, client):
        """Tests the GET method for Recipe resource item.
        Checks that the request returns a correct status code (200).
        Checks that all of the expected attributes and controls are present, and that the controls work.
        Checks that all of the items from the DB population are present, and their controls.
        """
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)
        assert body["name"] == "test-recipe-1"
        assert body["instruction"] == "testinstructions"
        assert body["main_ingredient_type"] == "dairy"
        _check_namespace(client, body)
        _check_profile_control("profile", client, body)
        _check_control_get_method("collection", client, body)
        _check_control_put_method("edit", client, body)
        _check_control_get_method("mealplanner:recipe_items-all", client, body)
        _check_control_delete_method("mealplanner:delete", client, body)
        resp = client.get(self.INVALID_URL)
        assert resp.status_code == 404

    def test_put(self, client):
        """Tests the PUT method for Recipe resource item.
        Checks that a valid request receives a 204 response.
        Checks that when name is changed, the recipe can be found from a its new URI.
        Checks all of the possible error codes.
        """

        valid = _get_recipe_json(number=1, main_ingredient_type='dairy')

        # test with wrong content type
        resp = client.put(self.RESOURCE_URL, data=json.dumps(valid))
        assert resp.status_code == 415

        resp = client.put(self.INVALID_URL, json=valid)
        assert resp.status_code == 404

        # test with another recipe's name
        valid["name"] = "test-recipe-2"
        resp = client.put(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 409

        # test with valid
        valid["name"] = "modified-recipe-1"
        resp = client.put(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 204

        # remove field for 400
        valid.pop("instruction")
        resp = client.put(self.RESOURCE_URL, json=valid)
        assert resp.status_code == 400

        valid = _get_recipe_json(number=1, main_ingredient_type='dairy')
        resp = client.put(self.RESOURCE_URL, json=valid)
        resp = client.get(self.MODIFIED_URL)
        assert resp.status_code == 200
        body = json.loads(resp.data)
        assert body["instruction"] == valid["instruction"]
        assert body["main_ingredient_type"] == valid["main_ingredient_type"]

    def test_delete(self, client):
        """Tests the DELETE method for Recipe resource item.
        Checks that a valid request receives the correct status code (204).
        Checks all of the possible error codes.
        """
        resp = client.delete(self.RESOURCE_URL)
        assert resp.status_code == 204
        resp = client.get(self.RESOURCE_URL)
        assert resp.status_code == 404
        resp = client.delete(self.INVALID_URL)
        assert resp.status_code == 404
