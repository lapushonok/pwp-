"""Test the database and the models

The following code is based on:
- Programmable Web Project -course Exercise 1 Testing Flask Applications:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/testing-flask-applications/
"""

import os
import pytest
import tempfile
from sqlalchemy.engine import Engine
from sqlalchemy import event
from sqlalchemy.exc import IntegrityError, StatementError

from meal_planner.api import app, db
from meal_planner.db.models import Ingredient, Recipe, RecipeItem, User, IngredientTypeEnum


@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()


@pytest.fixture
def db_handle():
    db_fd, db_fname = tempfile.mkstemp()
    app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///" + db_fname
    app.config["TESTING"] = True

    with app.app_context():
        db.create_all()

    yield db

    db.session.remove()
    os.close(db_fd)
    os.unlink(db_fname)


def _get_ingredient(number=1):
    types = IngredientTypeEnum
    return Ingredient(
        name="test_ingredient_{}".format(number),
        calories=30,
        type=types.fruit
    )


def _get_recipe(number=1):
    ingredient_types = IngredientTypeEnum
    return Recipe(
        name="test_recipe_{}".format(number),
        instruction="Description of cooking according to this recipe",
        main_ingredient_type=ingredient_types.fish
    )


def _get_recipe_item():
    return RecipeItem(
        amount=30.0,
        measure="kilogram",
    )


def _get_user():
    return User(
        name="test_user"
    )


def test_create_instances(db_handle):
    """
    Test to save one instance of the every model to the database using valid values for all columns.
    After saving the instances, test that every instances can be found from the database,
    and their relationships have been saved correctly.
    """

    # Create one instance from each model
    ingredient = _get_ingredient()
    recipe = _get_recipe()
    recipe_item = _get_recipe_item()
    user = _get_user()

    # Set relationships and add to the database
    recipe.recipe_items.append(recipe_item)
    recipe_item.ingredient = ingredient
    user.favourite_recipes.append(recipe)
    db_handle.session.add(ingredient)
    db_handle.session.add(recipe_item)
    db_handle.session.add(recipe)
    db_handle.session.add(user)
    db_handle.session.commit()

    # Check every instance exist in the database
    assert Ingredient.query.count() == 1
    assert Recipe.query.count() == 1
    assert RecipeItem.query.count() == 1
    assert User.query.count() == 1
    db_ingredient = Ingredient.query.first()
    db_recipe = Recipe.query.first()
    db_recipe_item = RecipeItem.query.first()
    db_user = User.query.first()

    # Check all relationships
    assert db_ingredient == db_recipe_item.ingredient
    assert db_recipe == db_recipe_item.recipe
    assert db_user in db_recipe.users
    assert db_recipe in db_user.favourite_recipes
    assert db_recipe_item in db_recipe.recipe_items
    assert db_recipe_item in db_ingredient.recipe_items


def test_ingredient_columns(db_handle):
    """
    Tests the types and restrictions of ingredient columns. Check that numerical values only accepts Float,
    unique value works correctly and required information exist like name and type
    """
    ingredient_1 = _get_ingredient()
    ingredient_2 = _get_ingredient()
    db_handle.session.add(ingredient_1)
    db_handle.session.add(ingredient_2)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()

    db_handle.session.rollback()

    ingredient = _get_ingredient()
    ingredient.calories = str(ingredient.calories) + "kcal"
    db_handle.session.add(ingredient)
    with pytest.raises(StatementError):
        db_handle.session.commit()

    db_handle.session.rollback()

    ingredient = _get_ingredient()
    ingredient.name = None
    db_handle.session.add(ingredient)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()

    db_handle.session.rollback()

    ingredient = _get_ingredient()
    ingredient.type = None
    db_handle.session.add(ingredient)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()


def test_recipe_columns(db_handle):
    """
    Tests the types and restrictions of recipe columns. Check that required information exist like name,
    instruction and main ingredient type
    """
    recipe = _get_recipe()
    recipe.name = None
    db_handle.session.add(recipe)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()

    db_handle.session.rollback()

    recipe = _get_recipe()
    recipe.instruction = None
    db_handle.session.add(recipe)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()

    db_handle.session.rollback()

    recipe = _get_recipe()
    recipe.main_ingredient_type = None
    db_handle.session.add(recipe)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()


def test_recipe_item_columns(db_handle):
    """
    Tests the types and restrictions of recipe item columns. Check that numerical values only accepts Float
    and required information exist like amount and measure
    """
    recipe_item = _get_recipe_item()
    recipe_item.amount = str(recipe_item.amount) + "tbsp"
    db_handle.session.add(recipe_item)
    with pytest.raises(StatementError):
        db_handle.session.commit()

    db_handle.session.rollback()

    recipe_item = _get_recipe_item()
    recipe_item.amount = None
    db_handle.session.add(recipe_item)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()

    db_handle.session.rollback()

    recipe_item = _get_recipe_item()
    recipe_item.measure = None
    db_handle.session.add(recipe_item)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()


def test_user_columns(db_handle):
    """
    Tests the types and restrictions of user columns. Check that unique value works correctly
    and required information exist like name
    """
    user_1 = _get_user()
    user_2 = _get_user()
    db_handle.session.add(user_1)
    db_handle.session.add(user_2)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()

    db_handle.session.rollback()

    user = _get_user()
    user.name = None
    db_handle.session.add(user)
    with pytest.raises(IntegrityError):
        db_handle.session.commit()

    db_handle.session.rollback()


def test_update_all_tables(db_handle):
    """Update information for every instance and save them to the database. Check that the update work correctly"""

    # Create one instance from each model
    ingredient = _get_ingredient()
    recipe = _get_recipe()
    recipe_item = _get_recipe_item()
    user = _get_user()

    # Set relationships and add to the database
    recipe.recipe_items.append(recipe_item)
    recipe_item.ingredient = ingredient
    user.favourite_recipes.append(recipe)
    db_handle.session.add(ingredient)
    db_handle.session.add(recipe_item)
    db_handle.session.add(recipe)
    db_handle.session.add(user)
    db_handle.session.commit()

    # Check every instance exist in the database
    assert Ingredient.query.count() == 1
    assert Recipe.query.count() == 1
    assert RecipeItem.query.count() == 1
    assert User.query.count() == 1

    db_ingredient = Ingredient.query.first()
    db_recipe = Recipe.query.first()
    db_recipe_item = RecipeItem.query.first()
    db_user = User.query.first()

    # Update ingredient instance
    db_ingredient.name = "Salmon"
    db_ingredient.calories = 356
    db_ingredient.type = "fish"
    db_handle.session.add(db_ingredient)
    db_handle.session.commit()

    # Check ingredient update
    db_ingredient_test = Ingredient.query.first()
    assert db_ingredient_test.name == "Salmon"
    assert db_ingredient_test.calories == 356
    assert db_ingredient_test.type == IngredientTypeEnum.fish

    # Update recipe instance
    db_recipe.name = "Oven salmon"
    db_recipe.instruction = "How to cook a salmon."
    db_recipe.main_ingredient_type = "fish"
    db_handle.session.add(db_recipe)
    db_handle.session.commit()

    # Check recipe update
    db_recipe_test = Recipe.query.first()
    assert db_recipe_test.name == "Oven salmon"
    assert db_recipe_test.instruction == "How to cook a salmon."
    assert db_recipe_test.main_ingredient_type == IngredientTypeEnum.fish

    # Update recipe item instance
    db_recipe_item.amount = 1
    db_recipe_item.measure = "fillet"
    db_handle.session.add(db_recipe_item)
    db_handle.session.commit()

    # Check recipe item update
    db_recipe_item_test = RecipeItem.query.first()
    assert db_recipe_item_test.amount == 1.0
    assert db_recipe_item_test.measure == "fillet"

    # Update user instance
    db_user.name = "User"
    db_handle.session.add(db_user)
    db_handle.session.commit()

    # Check  user update
    db_user_test = User.query.first()
    assert db_user_test.name == "User"


def test_delete_models(db_handle):
    """Test to delete models successfully from the database"""

    # Create one instance from each model
    ingredient = _get_ingredient()
    recipe = _get_recipe()
    recipe_item = _get_recipe_item()
    user = _get_user()

    # Set relationships and add to the database
    recipe.recipe_items.append(recipe_item)
    recipe_item.ingredient = ingredient
    user.favourite_recipes.append(recipe)
    db_handle.session.add(ingredient)
    db_handle.session.add(recipe_item)
    db_handle.session.add(recipe)
    db_handle.session.add(user)
    db_handle.session.commit()

    # Check every instance exist in the database
    assert Ingredient.query.count() == 1
    assert Recipe.query.count() == 1
    assert RecipeItem.query.count() == 1
    assert User.query.count() == 1

    db_ingredient = Ingredient.query.first()
    db_recipe = Recipe.query.first()
    db_user = User.query.first()

    # Delete ingredient
    db_handle.session.delete(db_ingredient)
    db_handle.session.commit()
    assert Ingredient.query.count() == 0

    # Delete recipe
    db_handle.session.delete(db_recipe)
    db_handle.session.commit()
    assert Recipe.query.count() == 0

    # Delete user
    db_handle.session.delete(db_user)
    db_handle.session.commit()
    assert User.query.count() == 0


def test_cascading_delete(db_handle):
    """Test cascade relation between recipe, recipe item and ingredient. A recipe can have multiple recipe item,
    but when a recipe is deleted, the recipe will disappear from the database.
    The delete cannot affect the ingredient. Vice versa, if an ingredient is deleted all recipe items disappear
    from recipes, but it does not delete the whole recipe."""

    # Create one instance from each model
    ingredient = _get_ingredient()
    ingredient_2 = _get_ingredient(2)
    recipe = _get_recipe()
    recipe_item = _get_recipe_item()
    recipe_item_2 = _get_recipe_item()

    # Set relationships and add to the database
    recipe.recipe_items.append(recipe_item)
    recipe.recipe_items.append(recipe_item_2)
    recipe_item.ingredient = ingredient
    recipe_item_2.ingredient = ingredient_2
    db_handle.session.add(ingredient)
    db_handle.session.add(ingredient_2)
    db_handle.session.add(recipe_item)
    db_handle.session.add(recipe_item_2)
    db_handle.session.add(recipe)

    # Check every instance exist in the database
    assert Ingredient.query.count() == 2
    assert Recipe.query.count() == 1
    assert RecipeItem.query.count() == 2

    # Delete recipe. It should delete recipe items but not the ingredients
    db_recipe = Recipe.query.first()
    db_handle.session.delete(db_recipe)
    db_handle.session.commit()
    assert Ingredient.query.count() == 2
    assert Recipe.query.count() == 0
    assert RecipeItem.query.count() == 0

    # Initialise db to test the case vice versa.
    recipe = _get_recipe()
    recipe_item = _get_recipe_item()
    recipe_item_2 = _get_recipe_item()
    recipe.recipe_items.append(recipe_item)
    recipe.recipe_items.append(recipe_item_2)
    recipe_item.ingredient = ingredient
    recipe_item_2.ingredient = ingredient_2
    db_handle.session.add(recipe_item)
    db_handle.session.add(recipe_item_2)
    db_handle.session.add(recipe)
    db_handle.session.commit()

    # Delete ingredient. It should delete its recipe item and the recipe item should not appear any more in the recipe.
    # Check length of recipe items of the recipe. It should be 2 before ingredient is deleted
    # and 1 after the ingredient is deleted
    db_ingredient = Ingredient.query.filter_by(name="test_ingredient_2").first()
    db_recipe = Recipe.query.first()
    assert db_ingredient.name == "test_ingredient_2"
    assert Ingredient.query.count() == 2
    assert Recipe.query.count() == 1
    assert RecipeItem.query.count() == 2
    assert len(db_recipe.recipe_items) == 2

    db_handle.session.delete(db_ingredient)
    db_handle.session.commit()

    db_recipe = Recipe.query.first()
    assert Ingredient.query.count() == 1
    assert Recipe.query.count() == 1
    assert RecipeItem.query.count() == 1
    assert len(db_recipe.recipe_items) == 1
