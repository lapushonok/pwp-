/* The following code is based on:
- Programmable Web Project course exercise 4:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/exercise-4-implementing-hypermedia-clients/
*/
const DEBUG = true;
const MASONJSON = "application/vnd.mason+json";
const PLAINJSON = "application/json";

function renderError(jqxhr) {
    let msg = jqxhr.responseJSON["@error"]["@message"];
    $("div.notification").html("<p class='error'>" + msg + "</p>");
}

function renderMsg(msg) {
    $("div.notification").html("<p class='msg'>" + msg + "</p>");
}

function getResource(href, renderer) {
    $.ajax({
        url: href,
        success: renderer,
        error: renderError
    });
}

function sendData(href, method, item, postProcessor) {

    $.ajax({
        url: href,
        type: method,
        data: JSON.stringify(item),
        contentType: PLAINJSON,
        processData: false,
        success: postProcessor,
        error: renderError
    });
}

function recipeRow(item) {
    let link = "<a href='" +
                item["@controls"].self.href +
                "' onClick='followLink(event, this, renderRecipe)'>show</a>";

    return "<tr><td>" + item.name +
            "</td><td>" + item.main_ingredient_type +
            "</td><td>" + link + "</td></tr>";
}


function ingredientRow(item) {
    return "<tr><td>" + item.name +
            "</td><td>" + item.calories +
            "</td><td>" + item.ingredient_type + "</td></tr>";
}

function recipeItemRow(item) {
    return "<tr><td>" + item.ingredient +
            "</td><td>" + item.amount +
            "</td><td>" + item.measure + "</td></tr>";
}

function getSubmittedRecipe(data, status, jqxhr) {
    renderMsg("Successful");
}

function getSubmittedIngredient(data, status, jqxhr) {
    renderMsg("Successful");
}

function getDeletedRecipe(data, status, jqxhr) {
    renderMsg("Successful");
}

function followLink(event, a, renderer) {
    event.preventDefault();
    getResource($(a).attr("href"), renderer);
}

function submitRecipe(event) {
    event.preventDefault();
    let data = {};
    let form = $("div.form form");
    data.name = $("input[name='name']").val();
    data.instruction = $("textarea[name='instruction']").val();
    data.main_ingredient_type = $("select[name='main_ingredient_type']").val();
    sendData(form.attr("action"), form.attr("method"), data, getSubmittedRecipe);
}

function submitIngredient(event) {
    event.preventDefault();
    let data = {};
    let form = $("div.form form");
    data.name = $("input[name='name']").val();
    if (Number.isNaN(parseFloat($("input[name='calories']").val())) == false){
        data.calories = parseFloat($("input[name='calories']").val());
    }
    data.type = $("select[name='type']").val();
    sendData(form.attr("action"), form.attr("method"), data, getSubmittedIngredient);
}

function deleteRecipe(event) {
    href = event.getAttribute('href');
    sendData(href, "DELETE", "", getDeletedRecipe);
}

function renderRecipeForm(ctrl) {
    let form = $("<form>");
    let name = ctrl.schema.properties.name;
    let instruction = ctrl.schema.properties.instruction;
    let main_ingredient_type = ctrl.schema.properties.main_ingredient_type;
    form.attr("action", ctrl.href);
    form.attr("method", ctrl.method);
    form.submit(submitRecipe);
    form.append("<p>" + ctrl.title + "</p>");
    form.append("<label>" + name.description + "</label>");
    form.append("<input type='text' name='name'>");
    form.append("<label>" + instruction.description + "</label>");
    form.append("<textarea type='text' name='instruction'>");
    form.append("<label>" + main_ingredient_type.description + "</label>");
    var types = ctrl.schema.properties.main_ingredient_type.enum;
    var selectList = document.createElement("select");
    selectList.name= "main_ingredient_type";
    form.append(selectList);
    for (var i = 0; i < types.length; i++) {
        var option = document.createElement("option");
        option.value = types[i];
        option.text = types[i];
        selectList.add(option);
    }
    ctrl.schema.required.forEach(function (property) {
        $("input[name='" + property + "']").attr("required", true);
    });
    form.append("<input type='submit' name='submit' value='Submit'>");
    $("div.form").html(form);
}

function renderIngredientForm(ctrl) {
    let form = $("<form>");
    let name = ctrl.schema.properties.name;
    let calories = ctrl.schema.properties.calories;
    let type = ctrl.schema.properties.type;
    form.attr("action", ctrl.href);
    form.attr("method", ctrl.method);
    form.submit(submitIngredient);
    form.append("<p>" + ctrl.title + "</p>");
    form.append("<label>" + name.description + "</label>");
    form.append("<input type='text' name='name'>");
    form.append("<label>" + calories.description + "</label>");
    form.append("<input type='number' name='calories'>");
    form.append("<label>" + type.description + "</label>");
    var types = ctrl.schema.properties.type.enum;
    var selectList = document.createElement("select");
    selectList.name= "type";
    form.append(selectList);
    for (var i = 0; i < types.length; i++) {
        var option = document.createElement("option");
        option.value = types[i];
        option.text = types[i];
        selectList.add(option);
    }
    ctrl.schema.required.forEach(function (property) {
        $("input[name='" + property + "']").attr("required", true);
    });
    form.append("<input type='submit' name='submit' value='Submit'>");
    $("div.form").html(form);
}

function renderRecipeItemForm(ctrl) {
    let form = $("<form>");
    let amount = ctrl.schema.properties.amount;
    let measure = ctrl.schema.properties.measure;
    form.attr("action", ctrl.href);
    form.attr("method", ctrl.method);
    $("div.form").html(form);
}

function renderRecipes(body) {
    $("div.notification").empty()
    $("div.page_title").empty().html("RECIPE COLLECTION");
    $("div.navigation").empty().html(
        "<a href='" + body["@controls"]["mealplanner:ingredients-all"].href
        + "' onClick='followLink(event, this, renderIngredients)'>Ingredients</a>"
    );

    renderRecipeForm(body["@controls"]["mealplanner:add-recipe"]);
    $("input[name='name']")
    $("textarea[name='instruction']")
    $("select[name='main_ingredient_type']")
    $(".resulttable thead").empty().html(
        "<tr><th>Name</th><th>Main ingredient type</th><th></tr>"
    );
    let tbody = $(".resulttable tbody");
    tbody.empty();
    $("div.table_header").empty().html("Recipes");
    body.items.forEach(function (item) {
        tbody.append(recipeRow(item));
    });
    $("div.delete-button").empty()
}

function renderRecipe(body) {
    $("div.notification").empty()
    $("div.page_title").empty().html("RECIPE");
    $("div.navigation").empty().html(
        "<a href='" + body["@controls"]["collection"].href
        + "' onClick='followLink(event, this, renderRecipes)'>Recipes</a>"
    );

    body["@controls"].edit.title = "Update or delete the recipe"
    renderRecipeForm(body["@controls"].edit);
    $("input[name='name']").val(body.name);
    $("textarea[name='instruction']").val(body.instruction);
    $("select[name='main_ingredient_type']").val(body.main_ingredient_type);
    $("form input[type='submit']")
    $(".resulttable tbody").empty()
    $(".table_header").empty()
    $(".resulttable thead").empty()

    $("div.page-bottom").append(
    "<a class='link' href='" +
    body["@controls"]["mealplanner:recipe_items-all"].href +
    "' onClick='followLink(event, this, renderRecipeItems)'></a>")
    $(".link").click()

    $("div.delete-button").append(
    "<br> <button href='" +
    body["@controls"]["mealplanner:delete"].href +
    "' onClick='deleteRecipe(this)'>Delete</a>")
}

function renderRecipeItems(body) {
    if (body.items.length != 0) {
        $(".table_header").empty().html("Recipe items");
        $(".resulttable thead").empty().html(
            "<tr><th>Ingredient</th><th>Amount</th><th>Measure</th></tr>"
        );
        let tbody = $(".resulttable tbody");
        tbody.empty();
        body.items.forEach(function (item) {
            tbody.append(recipeItemRow(item));
        })
    } else {
        $(".table_header").empty().html("No recipe items");
    }

}

function renderIngredients(body) {
    $("div.notification").empty()
    $("div.page_title").empty().html("INGREDIENT COLLECTION");
    $("div.navigation").empty().html(
        "<a href='" + body["@controls"]["mealplanner:recipes-all"].href
        + "' onClick='followLink(event, this, renderRecipes)'>Recipes</a>"
    );
    renderIngredientForm(body["@controls"]["mealplanner:add-ingredient"]);
    $("input[name='name']")
    $("input[name='calories']")
    $("select[name='type']")
    $(".resulttable thead").empty().html(
        "<tr><th>Name</th><th>Calories</th><th>Ingredient type</th><th></tr>"
    );
    let tbody = $(".resulttable tbody");
    tbody.empty();
    $("div.table_header").empty().html("Ingredients");
    body.items.forEach(function (item) {
        tbody.append(ingredientRow(item));
    });
    $("div.delete-button").empty()
}

$(document).ready(function () {
    getResource("http://127.0.0.1:5000/api/recipes/", renderRecipes);
});
