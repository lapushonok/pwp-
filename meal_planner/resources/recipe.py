"""Module for Recipe and RecipeCollection resources.

The following code is based on:
- Programmable Web project course exercise 3 at:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""
import json

from flask import Response, request, url_for
from flask_restful import Resource
from jsonschema import validate, ValidationError
from sqlalchemy import exc

from meal_planner.db.database import db
from meal_planner.db.models import Recipe
from meal_planner.utils.masonbuilder import create_error_response
from meal_planner.utils.meal_planner_builder import MealPlannerBuilder


class RecipeCollection(Resource):
    """Class for implementation of RecipeCollection-resource and related HTTP methods."""

    @staticmethod
    def get():
        """GET HTTP-method implementation for RecipeCollection-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200).
        """
        body = MealPlannerBuilder()
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.recipecollection"))
        body.add_control_users()
        body.add_control_ingredients()
        body.add_control_add_recipe()
        items = []
        for item in Recipe.query.all():
            item_body = MealPlannerBuilder(
                name=item.name,
                main_ingredient_type=item.main_ingredient_type.name
            )
            item_body.add_control("self", url_for("api.reciperesourceitem", recipe=item.name))
            item_body.add_control("profile", "/profiles/recipe/")
            items.append(item_body)
        body["items"] = items
        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def post():
        """POST HTTP-method implementation for RecipeCollection-resource.
        Creates a new resource item (Recipe) for RecipeCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (201, 400, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.recipe_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )
        request_body = request.json
        name = request_body['name']
        instruction = request_body['instruction']
        main_ingredient_type = request_body['main_ingredient_type']

        if Recipe.query.filter_by(name=name).first():
            return create_error_response(409,
                                         "Conflict",
                                         "Name already exists."
                                         )
        recipe = Recipe(
            name=name,
            instruction=instruction,
            main_ingredient_type=main_ingredient_type
        )
        db.session.add(recipe)
        db.session.commit()
        recipe_uri = url_for("api.reciperesourceitem", recipe=recipe.name)
        return Response(status=201, headers={"Location": recipe_uri})


class RecipeResourceItem(Resource):
    """Class for implementation of Recipe-resource and related HTTP methods."""

    @staticmethod
    def get(recipe):
        """GET HTTP-method implementation for Recipe-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200 or 404).
        """
        db_recipe = Recipe.query.filter_by(name=recipe).first()
        if db_recipe is None:
            return create_error_response(404,
                                         "Not found",
                                         "No recipe was found with the name {}".format(recipe)
                                         )
        body = MealPlannerBuilder(
            name=db_recipe.name,
            instruction=db_recipe.instruction,
            main_ingredient_type=db_recipe.main_ingredient_type.name
        )
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.reciperesourceitem", recipe=recipe))
        body.add_control("profile", "/profiles/recipe/")
        body.add_control("collection", url_for("api.recipecollection"))
        body.add_control_recipe_items(recipe_name=recipe)
        body.add_control_edit_recipe(recipe=recipe)
        body.add_control_delete_recipe(recipe=recipe)

        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def put(recipe):
        """PUT HTTP-method implementation for Recipe-resource.
        Modifies an existing resource item (Recipe) in RecipeCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204, 400, 404, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.recipe_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )
        request_body = request.json
        new_name = request_body['name']
        new_instruction = request_body['instruction']
        new_main_ingredient_type = request_body['main_ingredient_type']

        if not Recipe.query.filter_by(name=recipe).first():
            return create_error_response(404,
                                         "Not found.",
                                         "A recipe with name {} does not exist.".format(recipe)
                                         )
        try:
            recipe_entry = Recipe.query.filter_by(name=recipe).first()
            recipe_entry.name = new_name
            recipe_entry.instruction = new_instruction
            recipe_entry.main_ingredient_type = new_main_ingredient_type
            db.session.commit()
        except exc.IntegrityError:
            return create_error_response(409,
                                         "Conflict.",
                                         "Name with value {} already exists.".format(recipe)
                                         )
        return Response("", 204, mimetype="application/vnd.mason+json")

    @staticmethod
    def delete(recipe):
        """DELETE HTTP-method implementation for Recipe-resource.
        Deletes the resource item (Recipe) from RecipeCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204 or 404).
        """
        db_recipe = Recipe.query.filter_by(name=recipe).first()
        if not db_recipe:
            return create_error_response(404,
                                         "Not found.",
                                         "A recipe with name {} does not exist.".format(recipe)
                                         )
        db_recipe = db.session.query(Recipe).filter(Recipe.name == recipe).first()
        db.session.delete(db_recipe)
        db.session.commit()
        return Response("", 204, mimetype="application/vnd.mason+json")
