"""Module for FavouriteRecipeCollection and FavouriteRecipe resources.

The following code is based on:
- Programmable Web project course exercise 3 at:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""
import json

from flask import Response, request, url_for
from flask_restful import Resource
from jsonschema import validate, ValidationError

from meal_planner.db.database import db
from meal_planner.db.models import Recipe, User
from meal_planner.utils.masonbuilder import create_error_response
from meal_planner.utils.meal_planner_builder import MealPlannerBuilder


class FavouriteRecipeCollection(Resource):
    """Class for implementation of FavouriteRecipeCollection-resource and related HTTP methods."""

    @staticmethod
    def get(user):
        """GET HTTP-method implementation for FavouriteRecipeCollection-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200 or 404).
        """
        body = MealPlannerBuilder()
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.favouriterecipecollection", user=user))
        body.add_control_add_favourite_recipe(user=user)
        body.add_control("mealplanner:owner", url_for("api.userresourceitem", user=user))
        items = []

        db_user = User.query.filter_by(name=user).first()
        if db_user is None:
            return create_error_response(404,
                                         "Not found",
                                         "No user was found with name {}".format(user)
                                         )

        for item in User.query.filter_by(id=db_user.id).first().favourite_recipes:
            item_body = MealPlannerBuilder(
                name=item.name,
                main_ingredient_type=item.main_ingredient_type.name
            )
            item_body.add_control("self", url_for("api.favouritereciperesourceitem", user=user, recipe=item.name))
            item_body.add_control("profile", "/profiles/recipe/")
            items.append(item_body)
        body["items"] = items
        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def post(user):
        """POST HTTP-method implementation for FavouriteRecipeCollection-resource.
        Creates a new item (FavouriteRecipe) for FavouriteRecipeCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (201, 400, 404, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.favourite_recipe_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )

        request_body = request.json
        recipe = request_body['recipe_name']

        db_user = User.query.filter_by(name=user).first()
        if db_user is None:
            return create_error_response(404,
                                         "Not found",
                                         "No user was found with name {}".format(user)
                                         )

        db_recipe = Recipe.query.filter_by(name=recipe).first()
        if db_recipe is None:
            return create_error_response(404,
                                         "Not found",
                                         "No recipe was found with name {}".format(recipe)
                                         )

        if db_recipe in db_user.favourite_recipes:
            return create_error_response(409,
                                         "Conflict",
                                         "The recipe already exists in the user's favourite recipes."
                                         )

        db_user.favourite_recipes.append(db_recipe)
        db.session.commit()
        favourite_recipe_uri = url_for("api.favouritereciperesourceitem", user=db_user.name, recipe=db_recipe.name)
        return Response(status=201, headers={"Location": favourite_recipe_uri})


class FavouriteRecipeResourceItem(Resource):
    """Class for implementation of FavouriteRecipe-resource and related HTTP methods."""

    @staticmethod
    def get(user, recipe):
        """GET HTTP-method implementation for FavouriteRecipe-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200 or 404).
        """
        db_user = User.query.filter_by(name=user).first()
        if db_user is None:
            return create_error_response(404,
                                         "Not found",
                                         "No user was found with the name {}".format(user)
                                         )
        user_favourite_recipes = db_user.favourite_recipes
        for db_recipe in user_favourite_recipes:
            if db_recipe.name == recipe:
                body = MealPlannerBuilder(
                    name=db_recipe.name,
                    instruction=db_recipe.instruction,
                    main_ingredient_type=db_recipe.main_ingredient_type.name
                )
                body.add_namespace("mealplanner", "/mealplanner/link-relations/")
                body.add_control("self", url_for("api.favouritereciperesourceitem", user=user, recipe=recipe))
                body.add_control("profile", "/profiles/recipe/")
                body.add_control("collection", url_for("api.favouriterecipecollection", user=user))
                body.add_control("up", url_for(("api.reciperesourceitem"), recipe=recipe))
                body.add_control("mealplanner:owner", url_for("api.userresourceitem", user=user))
                body.add_control_delete_favourite_recipe(user=user, recipe=recipe)
                return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

        return create_error_response(404,
                                     "Not found",
                                     "No recipe was found with the name: {} "
                                     "for user with name: {}".format(recipe, user)
                                     )

    @staticmethod
    def delete(user, recipe):
        """DELETE HTTP-method implementation for FavouriteRecipe-resource.
        Deletes the resource item (FavouriteRecipe) from FavouriteRecipeCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204 or 404).
        """
        db_user = User.query.filter_by(name=user).first()
        if db_user is None:
            return create_error_response(404,
                                         "Not found",
                                         "No user was found with the name {}".format(user)
                                         )
        user_favourite_recipes = db_user.favourite_recipes
        for db_recipe in user_favourite_recipes:
            if db_recipe.name == recipe:
                user_favourite_recipes.remove(db_recipe)
                db.session.commit()
                return Response("", 204, mimetype="application/vnd.mason+json")
        return create_error_response(404,
                                     "Not found",
                                     "No recipe was found with the name: {}"
                                     "for user with name: {}".format(recipe, user)
                                     )
