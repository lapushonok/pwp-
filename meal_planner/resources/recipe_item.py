"""Module for RecipeItem and RecipeItemCollection resources.

The following code is based on:
- Programmable Web project course exercise 3 at:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""
import json

from flask import Response, request, url_for
from flask_restful import Resource
from jsonschema import validate, ValidationError

from meal_planner.db.database import db
from meal_planner.db.models import Recipe, RecipeItem, Ingredient
from meal_planner.utils.masonbuilder import create_error_response
from meal_planner.utils.meal_planner_builder import MealPlannerBuilder


class RecipeItemCollection(Resource):
    """Class for implementation of RecipeItemCollection-resource and related HTTP methods."""

    @staticmethod
    def get(recipe):
        """GET HTTP-method implementation for RecipeItemCollection-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200).
        """
        body = MealPlannerBuilder()
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.recipeitemscollection", recipe=recipe))
        body.add_control("up", url_for("api.reciperesourceitem", recipe=recipe))
        body.add_control_add_recipe_item(recipe_name=recipe)
        items = []
        recipe_db = Recipe.query.filter_by(name=recipe).first()

        for item in RecipeItem.query.filter_by(recipe_id=recipe_db.id).all():
            ingredient_db = Ingredient.query.filter_by(id=item.ingredient_id).first()
            item_body = MealPlannerBuilder(
                recipe=recipe_db.name,
                ingredient=ingredient_db.name,
                amount=item.amount,
                measure=item.measure
            )

            item_body.add_control("self",
                                  url_for("api.recipeitemresourceitem", recipe=recipe, recipeitem=ingredient_db.name))
            item_body.add_control("profile", "/profiles/recipeitem/")
            items.append(item_body)
        body["items"] = items
        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def post(recipe):
        """POST HTTP-method implementation for RecipeItemCollection-resource.
        Creates a new resource item (RecipeItem) for RecipeItemCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (201, 400, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.recipe_item_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )
        request_body = request.json
        ingredient_name = request_body['ingredient']
        amount = request_body['amount']
        measure = request_body['measure']

        recipe_id = Recipe.query.filter_by(name=recipe).first().id
        ingredient_id = Ingredient.query.filter_by(name=ingredient_name).first().id

        if RecipeItem.query.filter_by(recipe_id=recipe_id, ingredient_id=ingredient_id).first():
            return create_error_response(409,
                                         "Conflict",
                                         "Name already exists."
                                         )
        recipe_item = RecipeItem(
            amount=amount,
            measure=measure,
            recipe_id=recipe_id,
            ingredient_id=ingredient_id
        )
        db.session.add(recipe_item)
        db.session.commit()
        recipe_uri = url_for("api.recipeitemresourceitem", recipe=recipe, recipeitem=ingredient_name)
        return Response(status=201, headers={"Location": recipe_uri})


class RecipeItemResourceItem(Resource):
    """Class for implementation of RecipeItem-resource and related HTTP methods."""

    @staticmethod
    def get(recipe, recipeitem):
        """GET HTTP-method implementation for RecipeItem-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200 or 404).
        """
        db_recipe = Recipe.query.filter_by(name=recipe).first()

        if db_recipe is None:
            return create_error_response(404,
                                         "Not found",
                                         "No recipe was found with the name {}".format(recipe)
                                         )

        db_ingredient = Ingredient.query.filter_by(name=recipeitem).first()

        if db_ingredient is None:
            return create_error_response(404,
                                         "Not found",
                                         "No recipe item was found with the name {}".format(recipeitem)
                                         )

        db_recipeitem = RecipeItem.query.filter_by(recipe_id=db_recipe.id, ingredient_id=db_ingredient.id).first()
        body = MealPlannerBuilder(
            ingredient=db_ingredient.name,
            recipe=db_recipe.name,
            amount=db_recipeitem.amount,
            measure=db_recipeitem.measure
        )
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.recipeitemresourceitem", recipe=recipe, recipeitem=recipeitem))
        body.add_control("profile", "/profiles/recipeitem/")
        body.add_control("collection", url_for("api.recipeitemscollection", recipe=recipe))
        body.add_control("up", url_for("api.reciperesourceitem", recipe=recipe))
        body.add_control_edit_recipe_item(recipe_name=recipe, ingredient_name=recipeitem)
        body.add_control_delete_recipe_item(recipe_name=recipe, ingredient_name=recipeitem)
        body.add_control_ingredient_info(ingredient_name=recipeitem)

        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def put(recipe, recipeitem):
        """PUT HTTP-method implementation for RecipeItem-resource.
        Modifies an existing resource item (RecipeItem) in RecipeItemCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204, 400, 404, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.recipe_item_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )

        recipe_entry = Recipe.query.filter_by(name=recipe).first()

        if not recipe_entry:
            return create_error_response(404,
                                         "Not found.",
                                         "A recipe with name {} does not exist.".format(recipe)
                                         )
        request_body = request.json
        new_ingredient = request_body['ingredient']
        new_amount = request_body['amount']
        new_measure = request_body['measure']

        ingredient_entry = Ingredient.query.filter_by(name=recipeitem).first()

        if not ingredient_entry:
            return create_error_response(404,
                                         "Not found.",
                                         "An ingredient with name {} does not exist.".format(recipeitem)
                                         )

        new_ingredient_entry = Ingredient.query.filter_by(name=new_ingredient).first()

        if not new_ingredient_entry:
            return create_error_response(404,
                                         "Not found.",
                                         "An ingredient with name {} does not exist.".format(new_ingredient)
                                         )

        recipe_item_entry = RecipeItem.query.filter_by(recipe_id=recipe_entry.id, ingredient_id=ingredient_entry.id).first()

        if not recipe_item_entry:
            return create_error_response(404,
                                         "Not found.",
                                         "Recipe item with name {} not exists in this recipe.".format(recipeitem)
                                         )

        recipe_item_entry.ingredient_id = new_ingredient_entry.id
        recipe_item_entry.amount = float(new_amount)
        recipe_item_entry.measure = new_measure
        db.session.commit()

        return Response("", 204, mimetype="application/vnd.mason+json")

    @staticmethod
    def delete(recipe, recipeitem):
        """DELETE HTTP-method implementation for RecipeItem-resource.
        Deletes the resource item (RecipeItem) from RecipeItemCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204 or 404).
        """
        db_recipe = Recipe.query.filter_by(name=recipe).first()
        if not db_recipe:
            return create_error_response(404,
                                         "Not found.",
                                         "A recipe with name {} does not exist.".format(recipe)
                                         )

        db_ingredient = Ingredient.query.filter_by(name=recipeitem).first()

        if not db_ingredient:
            return create_error_response(404,
                                         "Not found.",
                                         "An ingredient item with name {} does not exist.".format(recipeitem)
                                         )

        db_recipeitem = RecipeItem.query.filter_by(recipe_id=db_recipe.id, ingredient_id=db_ingredient.id).first()
        if not db_recipeitem:
            return create_error_response(404,
                                         "Not found.",
                                         "A recipe item with name {} does not exist.".format(recipeitem)
                                         )
        db_recipeitem = db.session.query(RecipeItem).filter_by(recipe_id=db_recipe.id, ingredient_id=db_ingredient.id).first()
        db.session.delete(db_recipeitem)
        db.session.commit()
        return Response("", 204, mimetype="application/vnd.mason+json")
