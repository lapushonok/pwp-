"""Module for Recipe and RecipeCollection resources.

The following code is based on:
- Programmable Web project course exercise 3 at:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""
import json

from flask import Response, request, url_for
from flask_restful import Resource
from jsonschema import validate, ValidationError
from sqlalchemy import exc

from meal_planner.db.database import db
from meal_planner.db.models import User
from meal_planner.utils.masonbuilder import create_error_response
from meal_planner.utils.meal_planner_builder import MealPlannerBuilder


class UserCollection(Resource):
    """Class for implementation of UserCollection-resource and related HTTP methods."""

    @staticmethod
    def get():
        """GET HTTP-method implementation for UserCollection-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200).
        """
        body = MealPlannerBuilder()
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.usercollection"))
        body.add_control_recipes()
        body.add_control_ingredients()
        body.add_control_add_user()
        items = []
        for item in User.query.all():
            item_body = MealPlannerBuilder(
                name=item.name
            )

            item_body.add_control("self", url_for("api.userresourceitem", user=item.name))
            item_body.add_control("profile", "/profiles/user/")
            items.append(item_body)
        body["items"] = items
        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def post():
        """POST HTTP-method implementation for UserCollection-resource.
        Creates a new resource item (User) for UserCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (201, 400, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.user_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )
        request_body = request.json
        name = request_body['name']
        if User.query.filter_by(name=name).first():
            return create_error_response(409,
                                         "Already exists",
                                         "API already contains {}.".format(name)
                                         )
        user = User(name=name)
        db.session.add(user)
        db.session.commit()
        user_uri = url_for("api.userresourceitem", user=user.name)
        return Response(status=201, headers={"Location": user_uri})


class UserResourceItem(Resource):
    """Class for implementation of User-resource and related HTTP methods."""

    @staticmethod
    def get(user):
        """GET HTTP-method implementation for User-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200 or 404).
        """
        db_user = User.query.filter_by(name=user).first()
        if db_user is None:
            return create_error_response(404,
                                         "Not found",
                                         "No user was found with name {}".format(user)
                                         )
        body = MealPlannerBuilder(
            name=db_user.name
        )
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.userresourceitem", user=user))
        body.add_control("profile", "/profiles/user/")
        body.add_control("collection", url_for("api.usercollection"))
        body.add_control_favourite_recipes(user=user)
        body.add_control_edit_user(user=user)
        body.add_control_delete_user(user=user)

        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def put(user):
        """PUT HTTP-method implementation for User-resource.
        Modifies an existing resource item (User) in UserCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204, 400, 404, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.user_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )
        request_body = request.json
        new_name = request_body['name']

        if not User.query.filter_by(name=user).first():
            return create_error_response(404,
                                         "Not found.",
                                         "No user was found with the name {}.".format(user)
                                         )
        try:
            user_entry = User.query.filter_by(name=user).first()
            user_entry.name = new_name
            db.session.commit()
        except exc.IntegrityError:
            return create_error_response(409,
                                         "Conflict.",
                                         "Name with value {} already exists.".format(user)
                                         )

        return Response("", 204, mimetype="application/vnd.mason+json")

    @staticmethod
    def delete(user):
        """DELETE HTTP-method implementation for User-resource.
        Deletes the resource item (User) from UserCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204 or 404).
        """
        db_user = User.query.filter_by(name=user).first()
        if not db_user:
            return create_error_response(404,
                                         "Not found.",
                                         "A user with name {} does not exist.".format(user)
                                         )
        db_user = db.session.query(User).filter(User.name == user).first()
        db.session.delete(db_user)
        db.session.commit()
        return Response("", 204, mimetype="application/vnd.mason+json")
