"""Module for RecipeItem and RecipeItemCollection resources.

The following code is based on:
- Programmable Web project course exercise 3 at:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""
import json

from flask import Response, request, url_for
from flask_restful import Resource
from jsonschema import validate, ValidationError
from sqlalchemy import exc

from meal_planner.db.database import db
from meal_planner.db.models import Ingredient
from meal_planner.utils.masonbuilder import create_error_response
from meal_planner.utils.meal_planner_builder import MealPlannerBuilder


class IngredientCollection(Resource):
    """Class for implementation of IngredientCollection-resource and related HTTP methods."""

    @staticmethod
    def get():
        """GET HTTP-method implementation for IngredientCollection-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200).
        """
        body = MealPlannerBuilder()
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.ingredientcollection"))
        body.add_control_add_ingredient()
        body.add_control_recipes()
        body.add_control_users()
        items = []
        for item in Ingredient.query.all():
            item_body = MealPlannerBuilder(
                name=item.name,
                ingredient_type=item.type.name,
                calories=item.calories
            )
            item_body.add_control("self", url_for("api.ingredientresourceitem", ingredient=item.name))
            item_body.add_control("profile", "/profiles/ingredient/")
            items.append(item_body)
        body["items"] = items
        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def post():
        """POST HTTP-method implementation for IngredientCollection-resource.
        Creates a new resource item (Ingredient) for IngredientCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (201, 400, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON."
                                         )
        try:
            validate(request.json, builder.ingredient_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body",
                                         "The request body is not valid"
                                         )
        request_body = request.json
        name = request_body['name']
        ingredient_type = request_body['type']
        try:
            calories = request_body['calories']
        except KeyError:
            calories = None

        if Ingredient.query.filter_by(name=name).first():
            return create_error_response(409,
                                         "Conflict",
                                         "Name {} already exists".format(name)
                                         )
        ingredient = Ingredient(
            name=name,
            type=ingredient_type,
            calories=calories
        )
        db.session.add(ingredient)
        db.session.commit()
        recipe_uri = url_for("api.ingredientresourceitem", ingredient=ingredient.name)
        return Response(status=201, headers={"Location": recipe_uri})


class IngredientResourceItem(Resource):
    """Class for implementation of Ingredient-resource and related HTTP methods."""

    @staticmethod
    def get(ingredient):
        """GET HTTP-method implementation for Ingredient-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (200 or 404).
        """
        db_ingredient = Ingredient.query.filter_by(name=ingredient).first()
        if db_ingredient is None:
            return create_error_response(404,
                                         "Not found",
                                         "No ingredient was found with the name {}".format(ingredient)
                                         )
        body = MealPlannerBuilder(
            name=db_ingredient.name,
            ingredient_type=db_ingredient.type.name,
            calories=db_ingredient.calories
        )
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control("self", url_for("api.ingredientresourceitem", ingredient=ingredient))
        body.add_control("profile", "/profiles/ingredient/")
        body.add_control("collection", url_for("api.ingredientcollection"))
        body.add_control_edit_ingredient(ingredient_name=ingredient)
        body.add_control_delete_ingredient(ingredient_name=ingredient)

        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")

    @staticmethod
    def put(ingredient):
        """PUT HTTP-method implementation for Ingredient-resource.
        Modifies an existing resource item (Ingredient) in IngredientCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204, 400, 404, 409, or 415).
        """
        builder = MealPlannerBuilder()
        if not request.get_json():
            return create_error_response(415,
                                         "Unsupported media type.",
                                         "Request must be JSON"
                                         )
        try:
            validate(request.json, builder.ingredient_schema())
        except ValidationError:
            return create_error_response(400,
                                         "Invalid request body.",
                                         "The request body is not valid."
                                         )
        request_body = request.json
        new_name = request_body['name']
        new_type = request_body['type']
        try:
            new_calories = request_body['calories']
        except KeyError:
            new_calories = None

        if not Ingredient.query.filter_by(name=ingredient).first():
            return create_error_response(404,
                                         "Not found.",
                                         "No ingredient was found with the name {}".format(ingredient)
                                         )
        try:
            ingredient_entry = Ingredient.query.filter_by(name=ingredient).first()
            ingredient_entry.name = new_name
            ingredient_entry.type = new_type
            ingredient_entry.calories = new_calories
            db.session.commit()
        except exc.IntegrityError:
            return create_error_response(409,
                                         "Conflict.",
                                         "Ingredient with name {} already exists.".format(ingredient)
                                         )
        return Response("", 204, mimetype="application/vnd.mason+json")

    @staticmethod
    def delete(ingredient):
        """DELETE HTTP-method implementation for Ingredient-resource.
        Deletes the resource item (Ingredient) from IngredientCollection.
        Constructs a MASON JSON response body with associated information.
        Returns the response with an appropriate HTTP code (204 or 404).
        """
        db_ingredient = Ingredient.query.filter_by(name=ingredient).first()
        if not db_ingredient:
            return create_error_response(404,
                                         "Not found.",
                                         "An ingredient with name {} does not exist.".format(ingredient)
                                         )
        db_ingredient = db.session.query(Ingredient).filter_by(name=ingredient).first()
        db.session.delete(db_ingredient)
        db.session.commit()
        return Response("", 204, mimetype="application/vnd.mason+json")