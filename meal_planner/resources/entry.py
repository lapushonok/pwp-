"""Module for Recipe and RecipeCollection resources.

The following code is based on:
- Programmable Web project course exercise 3 at:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""

import json

from flask import Response
from flask_restful import Resource
from meal_planner.utils.meal_planner_builder import MealPlannerBuilder


class Entry(Resource):
    """Class for implementation of Entry-resource and related HTTP methods."""
    @staticmethod
    def get():
        """GET HTTP-method implementation for Entry-resource.
        Returns information of the resource.
        Constructs a MASON JSON response body with associated information.
        Returns the response with HTTP code 200.
        """
        body = MealPlannerBuilder()
        body.add_namespace("mealplanner", "/mealplanner/link-relations/")
        body.add_control_users()
        body.add_control_recipes()
        body.add_control_ingredients()
        return Response(json.dumps(body), 200, mimetype="application/vnd.mason+json")
