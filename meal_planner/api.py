"""Module for Meal Planner API initialization.

The following code is based on:
- Programmable Web project course exercise 3 at:
https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/implementing-rest-apis-with-flask/
"""
from flask import Flask, redirect
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from meal_planner.db import database

app = Flask(__name__, static_folder="static")
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db/meal_planner.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
database.db = db
api = Api(app)

APIARY_URL = "https://mealplanner1.docs.apiary.io/#"

# Resource module imports here for avoiding circular imports
from meal_planner.resources.favourite_recipe import FavouriteRecipeCollection, FavouriteRecipeResourceItem
from meal_planner.resources.recipe import RecipeCollection, RecipeResourceItem
from meal_planner.resources.user import UserCollection, UserResourceItem
from meal_planner.resources.recipe_item import RecipeItemCollection, RecipeItemResourceItem
from meal_planner.resources.ingredient import IngredientCollection, IngredientResourceItem
from meal_planner.resources.entry import Entry

# Resource endpoint definitions
api.add_resource(Entry, "/api/", endpoint="api.entrypoint")

api.add_resource(FavouriteRecipeCollection, "/api/users/<user>/favourite-recipes/", endpoint="api.favouriterecipecollection")
api.add_resource(FavouriteRecipeResourceItem, "/api/users/<user>/favourite-recipes/<recipe>/", endpoint="api.favouritereciperesourceitem")

api.add_resource(RecipeCollection, "/api/recipes/", endpoint="api.recipecollection")
api.add_resource(RecipeResourceItem, "/api/recipes/<recipe>/", endpoint="api.reciperesourceitem")

api.add_resource(UserCollection, "/api/users/", endpoint="api.usercollection")
api.add_resource(UserResourceItem, "/api/users/<user>/", endpoint="api.userresourceitem")

api.add_resource(RecipeItemCollection, "/api/recipes/<recipe>/recipeitems/", endpoint="api.recipeitemscollection")
api.add_resource(RecipeItemResourceItem, "/api/recipes/<recipe>/recipeitems/<recipeitem>/", endpoint="api.recipeitemresourceitem")

api.add_resource(IngredientCollection, "/api/ingredients/", endpoint="api.ingredientcollection")
api.add_resource(IngredientResourceItem, "/api/ingredients/<ingredient>/", endpoint="api.ingredientresourceitem")


@app.route("/mealplanner/link-relations/")
def redirect_to_apiary_link_relations():
    """Redirect from link relations -resource to Apiary."""
    return redirect(APIARY_URL + "reference/link-relations")


@app.route("/profiles/user/")
def redirect_to_apiary_user_profile():
    """Redirect from User profile -resource to Apiary."""
    return redirect(APIARY_URL + "reference/profiles")


@app.route("/profiles/recipe/")
def redirect_to_apiary_recipe_profile():
    """Redirect from Recipe profile -resource to Apiary."""
    return redirect(APIARY_URL + "reference/profiles")


@app.route("/profiles/recipeitem/")
def redirect_to_apiary_recipeitem_profile():
    """Redirect from RecipeItem profile -resource to Apiary."""
    return redirect(APIARY_URL + "reference/profiles")


@app.route("/profiles/ingredient/")
def redirect_to_apiary_ingredient_profile():
    """Redirect from Ingredient profile -resource to Apiary."""
    return redirect(APIARY_URL + "reference/profiles")


@app.route("/client/")
def admin_site():
    """Endpoint for accessing Meal Planner client."""
    return app.send_static_file("html/client.html")


if __name__ == "__main__":
    db.create_all()
    app.run(debug=True)
