"""Module for database model classes.

The following code is based on:
- Programmable Web Project -course Exercise 1:
  https://lovelace.oulu.fi/ohjelmoitava-web/ohjelmoitava-web/introduction-to-web-development/
- https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html
"""
import enum

from meal_planner.db.database import db


# Association table for many-to-many relationship between users and favourite recipes
favourite_recipes = db.Table("favourite_recipes",
                             db.Column("user_id",
                                       db.Integer,
                                       db.ForeignKey("user.id"),
                                       primary_key=True),
                             db.Column("recipe_id",
                                       db.Integer,
                                       db.ForeignKey("recipe.id"),
                                       primary_key=True)
                             )


class IngredientTypeEnum(enum.Enum):
    """Enum definition for the model variable Recipe.main_ingredient and Ingredient.type"""
    berry = 0
    dairy = 1
    egg = 2
    fish = 3
    fruit = 4
    grain = 5
    poultry = 6
    red_meat = 7
    rice = 8
    sausage = 9
    seafood = 10
    vegetable = 11
    miscellaneous = 12


class Ingredient(db.Model):
    """Class for the database model definition for Ingredient."""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    calories = db.Column(db.Float, nullable=True)
    type = db.Column(db.Enum(IngredientTypeEnum), nullable=False)

    recipe_items = db.relationship("RecipeItem", cascade="all,delete,delete-orphan", back_populates="ingredient")


class Recipe(db.Model):
    """Class for the database model definition for Recipe."""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)
    instruction = db.Column(db.String, nullable=False)
    main_ingredient_type = db.Column(db.Enum(IngredientTypeEnum), nullable=False)

    recipe_items = db.relationship("RecipeItem", cascade="delete,delete-orphan", back_populates="recipe")
    users = db.relationship("User", secondary=favourite_recipes, back_populates="favourite_recipes")


class RecipeItem(db.Model):
    """Class for the database model definition for RecipeItem."""
    id = db.Column(db.Integer, primary_key=True)
    amount = db.Column(db.Float, nullable=False)
    measure = db.Column(db.String, nullable=False)
    recipe_id = db.Column(db.Integer, db.ForeignKey("recipe.id"))
    ingredient_id = db.Column(db.Integer, db.ForeignKey("ingredient.id"))

    recipe = db.relationship("Recipe", back_populates="recipe_items")
    ingredient = db.relationship("Ingredient", back_populates="recipe_items")


class User(db.Model):
    """Class for the database model definition for User."""
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, unique=True, nullable=False)

    favourite_recipes = db.relationship("Recipe", secondary=favourite_recipes, back_populates="users")
