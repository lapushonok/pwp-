"""Module for initializing database for application wide use."""
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()
